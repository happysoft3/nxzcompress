
#ifndef XZ_INTERFACE_H__
#define XZ_INTERFACE_H__

#include "ccobject.h"

#include <string>
#include <mutex>
#include <atomic>

class LzCompression;
class CIniManager;

class XZInterface : public CCObject
{
private:
    std::list<std::string> m_filelist;
    std::string m_currentfile;
    std::unique_ptr<LzCompression> m_compress;
    std::string m_appPath;

    int m_progPercent;

    std::list<std::tuple<int, int>> m_procRateList;

    EventSignal<bool> m_onStoppedPerformance;
    std::unique_ptr<CIniManager> m_iniFile;

    bool m_isCreateDir;

    std::atomic<bool> m_shutdown;

public:
    explicit XZInterface(char **filelist, int listcount);
    ~XZInterface() override;

    bool RunInterface();
    bool SetAppPath(const std::string &fullpath);
    void ShowFileList();
    void ReadOpt();

private:
    bool IsNxz();
    bool InnerProgress(std::string &&filename);
    std::string OnlyFilename();
    std::string OnlyFileExtension();

    bool FileListTraveling(std::function<bool(std::string &&)> &&in);

    bool WriteOutput(bool compressMode = false);

    void ShowPerformance();
    void NotificationProgress(int cur, int max);
    void StopPerfomance(bool stat);

private:
    std::mutex m_lock;
};

#endif

