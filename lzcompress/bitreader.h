
#ifndef BIT_READER_H__
#define BIT_READER_H__

#include "bitwiseio.h"
#include <stdint.h>

template <class T>
class BitReader : public BitWiseIO<T>
{
private:
	uint32_t m_bitpos;
	uint32_t m_bufpos;
	T m_bitbuf;
	const T *m_source;

public:
	explicit BitReader()
        : BitWiseIO<T>()
	{
		m_bitpos = 0;
		m_bufpos = 0;
		m_source = nullptr;
	}
	virtual ~BitReader() override
	{ }

	template <typename Container>
	bool SetBuffer(const Container &c)
	{
		if (c.empty())
			return false;

		m_source = c.data();
		return true;
	}

    bool SetBuffer(const T *const &ptr)
    {
        m_source = ptr;
        return true;
    }

	int Readbit()
	{
		if (m_bitpos <= 0)
		{
			m_bitbuf = m_source[m_bufpos++];
			m_bitpos = this->bit_count;
		}

		int result = (m_bitbuf & 0x80) >> 7;

		m_bitbuf <<= 1;
		--m_bitpos;
		return result;
	}

	int Read(int bitcount)
	{
		int result = 0;

		if (bitcount <= static_cast<int>(m_bitpos))
		{
			result = m_bitbuf >> (this->bit_count - bitcount);
			m_bitbuf <<= bitcount;
			m_bitpos -= bitcount;
			return result;
		}

		for (int i = 0; i < bitcount; i++)
			result = (result << 1) | Readbit();
		return result;
	}
};

#endif

