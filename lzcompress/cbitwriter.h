
#ifndef C_BIT_WRITER_H__
#define C_BIT_WRITER_H__

#include "bitwiseio.h"

template <typename T>
class CBitWriter : public BitWiseIO<T>
{
private:
    uint32_t m_bitpos;
    std::vector<T> m_outvector;
    size_t m_srclength;
    T m_bitBuff;

public:
    explicit CBitWriter(size_t srclength);
    virtual ~CBitWriter() override;

public:
    inline bool EndWrite();
    void Write(int value, int length = 1);

    template <typename DestContainer>
    void GetResult(DestContainer &destcont)
    {
        destcont = DestContainer(m_outvector.begin(), m_outvector.end());
    }

};

template <typename T>
CBitWriter<T>::CBitWriter(size_t srclength)
    : BitWiseIO<T>()
{
    m_bitpos = 0;
    m_bitBuff = 0;
    m_srclength = srclength;
    m_outvector.reserve(srclength);
    this->PushNumberBack(srclength, m_outvector);
}

template <typename T>
CBitWriter<T>::~CBitWriter()
{ }

template <typename T>
bool CBitWriter<T>::EndWrite()
{
    if (m_bitpos > 0)
        m_outvector.push_back((m_bitBuff << (this->bit_count - m_bitpos)) & 0xff);

    return true;
}

template <typename T>
void CBitWriter<T>::Write(int value, int length)
{
    while (length > 0)
    {
        if (m_bitpos >= this->bit_count)
        {
            m_outvector.push_back(m_bitBuff);
            m_bitpos = 0;
            m_bitBuff = 0;
        }
        --length;
        m_bitBuff <<= 1;
        m_bitBuff |= (((value & (1 << length)) >> length) & 0xff);
        ++m_bitpos;
    }
}

#endif

