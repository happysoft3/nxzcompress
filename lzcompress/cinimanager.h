
#ifndef C_INI_MANAGER_T_H__
#define C_INI_MANAGER_T_H__

#include <string>
#include <array>
#include <sstream>

class CIniManager
{
    static constexpr size_t m_constMax = 1024;

private:
    std::string m_path;
    std::array<char, m_constMax> m_largebuffer;

public:
    explicit CIniManager(const std::string &path);
    ~CIniManager();

private:
    bool readFromFile(const std::string &keyname, const std::string &itemname);
    bool writeToFile(const std::string &keyname, const std::string &itemname);

    template <class _Ty>
    struct IsValidChar
    {
        static constexpr bool value = std::_Is_any_of_v<std::remove_cv_t<_Ty>, char, unsigned char>;
    };

    template <bool IsArray>
    struct BufferPrivateT
    {
        template <typename T, class = std::enable_if<std::is_arithmetic<T>::value>::type>
        static std::string outPrivate(const T &src)
        {
            return std::to_string(src);
        }
    };

    template <>
    struct BufferPrivateT<true>
    {
        template <typename T, class = std::enable_if<IsValidChar<T>::value>::type>
        static std::string outPrivate(const T *src)
        {
            return std::string(src);
        }
    };

    template <typename T, class = std::enable_if<std::is_arithmetic<T>::value>::type>
    bool setValue(T &dest)
    {
        char c;

        std::istringstream is(m_largebuffer.data());
        T res;

        if ((is >> res) && (!(is >> c)))
        {
            dest = res;
            return true;
        }
        return false;
    }

    bool setValue(std::string &dest)
    {
        dest = m_largebuffer.data();

        return true;
    }

    template <typename T>
    bool writeToArray(const T &src)
    {
        return writeToArray(BufferPrivateT<std::is_array<T>::value>::outPrivate(src));
    }

    template <>
    bool writeToArray(const std::string &src)
    {
        if (src.length() > m_largebuffer.size())

            return false;

        m_largebuffer.fill('\0');
        std::copy(src.begin(), src.end(), m_largebuffer.begin());

        return true;
    }

public:
    template <typename T>
    bool GetValue(const std::string &keyname, const std::string &itemname, T &dest)
    {
        if (readFromFile(keyname, itemname))
            return setValue(dest);

        return false;
    }

    bool RemoveKey(const std::string &keyname);
    bool RemoveItem(const std::string &keyname, const std::string &itemname);
    bool GetValue(const std::string &keyname, const std::string &itemname, char *pDest, size_t destlength);

    template <typename T>
    bool SetValue(const std::string &keyname, const std::string &itemname, const T &src)
    {
        if (!writeToArray(src))
            return false;

        return writeToFile(keyname, itemname);
    }
};

#endif

