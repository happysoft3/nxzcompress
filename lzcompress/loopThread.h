
#ifndef NET_THREAD_H__
#define NET_THREAD_H__

#include "ccobject.h"
#include <thread>
#include <functional>
#include <mutex>

class LoopThread : public CCObject
{
private:
    std::thread m_thread;
    std::condition_variable m_condvar;
    bool m_terminated;
    std::function<bool()> m_waitCondition;
    std::function<void()> m_waitFunction;
    std::function<void()> m_taskFunction;

public:
    LoopThread();
    ~LoopThread() override;

private:
    void WaitCondition();
    void DoTask(std::function<void()> task);
    bool OnInitialize();
    void OnDeinitialize();
    bool OnStarted();
    void OnStopped();

public:
    void SetWaitCondition(std::function<bool()> &&cond);
    void SetTaskFunction(std::function<void()> &&task);
    void Notify();

    bool Start();
    void Stop();

private:
    std::mutex m_waitLock;
};

#endif

