
#ifndef BIT_WRITER_H__
#define BIT_WRITER_H__

#include "bitwiseio.h"

template <typename T>
class BitWriter : public BitWiseIO<T>
{
private:
	uint32_t m_bitIndex;
	uint32_t m_bitpos;
	std::list<T> m_outputList;
    size_t m_srclength;
	T m_bitBuff;

public:
	explicit BitWriter(size_t srclength);
	virtual ~BitWriter() override;

public:
	inline bool EndWrite();
	void Write(int value, int length = 1);

	template <typename DestContainer>
	void GetResult(DestContainer &destcont)
	{
		destcont = DestContainer(m_outputList.begin(), m_outputList.end());
	}
	
};

template <typename T>
BitWriter<T>::BitWriter(size_t srclength)
    : BitWiseIO<T>()
{
	m_bitIndex = 0;
	m_bitpos = 0;
	m_bitBuff = 0;
    m_srclength = srclength;
}

template <typename T>
BitWriter<T>::~BitWriter()
{ }

template <typename T>
bool BitWriter<T>::EndWrite()
{
	if (m_bitpos > 0)
		m_outputList.push_back((m_bitBuff << (this->bit_count - m_bitpos)) & 0xff);
    return PushNumberToList(m_srclength, m_outputList);
}

template <typename T>
void BitWriter<T>::Write(int value, int length)
{
	while (length > 0)
	{
		if (m_bitpos >= this->bit_count)
		{
			m_outputList.push_back(m_bitBuff);
			m_bitpos = 0;
			m_bitBuff = 0;
		}
		--length;
		m_bitBuff <<= 1;
		m_bitBuff |= (((value & (1 << length)) >> length) & 0xff);
		++m_bitpos;
	}
}



#endif

