
#ifndef LZ_COMPRESSION_H__
#define LZ_COMPRESSION_H__

#include "ccobject.h"

#include <vector>
#include <memory>

template <typename T>
class CBitWriter;

template <typename T>
class BitReader;

class LzCompression
{
private:
	const int m_symbols;
	const int m_windowsize;
	const int m_rebuildcount;

	std::vector<int> m_symbolAlphabets;
	std::vector<int> m_symbolcounts;

    const uint8_t *m_pInputptr;
    int m_srcLength;

    std::vector<uint8_t> m_outputstream;

	std::unique_ptr<CBitWriter<uint8_t>> m_bitwr;
	std::unique_ptr<BitReader<uint8_t>> m_bitrd;

	std::vector<int> m_lzwindow;
	int m_lzposwindow;
    int m_workpos;
    int m_cursymb;

    int m_longestMatchLen;

private:
	explicit LzCompression(int symbols, int windowsize);

public:
	explicit LzCompression();
	~LzCompression();

private:
	template <class T>
	inline int MakeIntWithWord(T highvalue, T lowvalue)
	{
		return (highvalue << 0x10) | lowvalue;
	}

	template <typename DestContainer, typename CheckerContainer>
	inline void IndexOfSymbolsRev(DestContainer &destcont, const CheckerContainer &checkcont)
	{
		typename CheckerContainer::const_reverse_iterator checkerIterator = checkcont.rbegin();
		typename DestContainer::reverse_iterator symbIterator = destcont.rbegin();
		int cmpkey = static_cast<int>(destcont.size());

		while ((--cmpkey) >= 0)
		{
			if (cmpkey != (*checkerIterator))
				*(symbIterator++) = cmpkey;
			else if (checkerIterator != checkcont.rend())
				++checkerIterator;
		}
	}

	void BuildInitialAlphabet();
	void RebuildAlphabet();
	void FrontWork(int &matchpos);
	void RepeatingFoundSequence();
	bool SearchForHuffmanAlphabet();
	void SymbolEqual();
	void SymbolGreater(int winoff);
	bool CompressImpl(const uint8_t *inputptr, size_t length);

	void DecompGetSymbol(const std::vector<int> &symbIndexTb);
	void DecompSymbLess();
	void DecompSymbExactly(std::vector<int> &symbIndexTb);
	void DecompSymbElse();
	bool DecompressImpl(const uint8_t *inputptr, uint32_t length);

private:
    void completedCompress();

public:

	template <typename SrcContainer>
	bool Compress(const SrcContainer &srcCont)
	{
		BuildInitialAlphabet();

		if (!CompressImpl(srcCont.data(), srcCont.size()))
			return false;

        completedCompress();
		return true;
	}

    template <typename DestContainer>
    void GetResult(DestContainer &destcont)
    {
        destcont = DestContainer(m_outputstream.begin(), m_outputstream.end());
    }

    template <typename SrcContainer>
    bool Decompress(const SrcContainer &srcCont)
    {
        m_workpos = 0;
        m_cursymb = 0;
        m_lzposwindow = 0;

        BuildInitialAlphabet();

        if (!DecompressImpl(srcCont.data(), srcCont.size()))
            return false;

        return true;
    }

	DECLARE_SIGNAL(OnProgressHit, int, int)

};

#endif

