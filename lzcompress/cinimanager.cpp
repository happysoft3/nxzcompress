
#include "cinimanager.h"
#include <windows.h>

CIniManager::CIniManager(const std::string &path)
{
    m_largebuffer = { };
    m_path = path;
}

CIniManager::~CIniManager()
{ }

bool CIniManager::readFromFile(const std::string &keyname, const std::string &itemname)
{
    ::GetPrivateProfileStringA(keyname.c_str(), itemname.c_str(), nullptr,
        m_largebuffer.data(), static_cast<DWORD>(m_largebuffer.max_size()), m_path.c_str());
    return ::GetLastError() == 0;
}

bool CIniManager::writeToFile(const std::string &keyname, const std::string &itemname)
{
    ::WritePrivateProfileStringA(keyname.c_str(), itemname.c_str(), m_largebuffer.data(), m_path.c_str());

    return ::GetLastError() == 0;
}

bool CIniManager::RemoveKey(const std::string &keyname)
{
    return ::WritePrivateProfileStringA(keyname.c_str(), NULL, NULL, m_path.c_str()) & 1;
}

bool CIniManager::RemoveItem(const std::string &keyname, const std::string &itemname)
{
    return ::WritePrivateProfileStringA(keyname.c_str(), itemname.c_str(), NULL, m_path.c_str()) & 1;
}

bool CIniManager::GetValue(const std::string &keyname, const std::string &itemname, char *pDest, size_t destlength)
{
    if (readFromFile(keyname, itemname))
    {
        strcpy_s(pDest, destlength, m_largebuffer.data());

        return true;
    }
    return false;
}