
#ifndef BIT_WISE_IO_H__
#define BIT_WISE_IO_H__

#include <vector>
#include <list>

template <typename T>
class BitWiseIO
{
protected:
    using value_type = T;
    static constexpr size_t bit_count = sizeof(value_type) << 3;

public:
	explicit BitWiseIO()
	{ }

	virtual ~BitWiseIO()
	{ }

protected:
    template <int N>
    struct Validate
    {
        static constexpr bool isvalid = false;
    };

    template <>
    struct Validate<0>
    {
        static constexpr bool isvalid = true;
    };

    template <typename T, typename U>
    bool PushNumberToList(T n, std::list<U> &destlist)
    {
        static constexpr bool rep_if = Validate<sizeof(T) % sizeof(U)>::isvalid;
        if (!rep_if)
            return rep_if;

        static constexpr int rep_count = sizeof(T) / sizeof(U);

        U *p = reinterpret_cast<U *>(&n);
        int rep = rep_count;

        while ((--rep) >= 0)
            destlist.push_front(p[rep]);
        return true;
    }

    template <typename T, typename DestContainer>
    bool PushNumberBack(T n, DestContainer &destcont)
    {
        using dest_valueType = typename DestContainer::value_type;
        static constexpr bool rep_if = Validate<sizeof(T) % sizeof(dest_valueType)>::isvalid;
        if (!rep_if)
            return rep_if;

        static constexpr int rep_count = sizeof(T) / sizeof(dest_valueType);

        dest_valueType *p = reinterpret_cast<dest_valueType *>(&n);
        int rep = -1;

        while ((++rep) < rep_count)
            destcont.push_back(p[rep]);
        return true;
    }
};

#endif
