
#include "xzinterface.h"
#include "lzcompression.h"

#include "stringhelper.h"

#include "cinimanager.h"
#include "loopThread.h"
#include <fstream>
#include <iterator>
//#include <thread>
#include <chrono>
#include <filesystem>

#define VISUAL_STUDIO_2015 1900
#if _MSC_VER == VISUAL_STUDIO_2015
#define NAMESPACE_FILESYSTEM std::experimental::filesystem
#else
#define NAMESPACE_FILESYSTEM std::filesystem
#endif
#undef VISUAL_STUDIO_2015

#include <iostream>

using namespace _StringHelper;

XZInterface::XZInterface(char **argv, int listcount)
{
    int rep = -1;

    while ((++rep) < listcount)
        m_filelist.emplace_back(argv[rep]);

    m_progPercent = 0;
    m_isCreateDir = false;

    m_onStoppedPerformance.Connection(&XZInterface::StopPerfomance, this);

    m_shutdown = false;
}

XZInterface::~XZInterface()
{ }

bool XZInterface::RunInterface()
{
    std::unique_ptr<LoopThread> perfmon(new LoopThread);

    perfmon->SetTaskFunction([this]() { this->ShowPerformance(); });
    if (!perfmon->Start())
        return false;

    FileListTraveling([this](std::string &&s)
    {
        return InnerProgress(std::forward<std::string>(s));
    });
    perfmon->Stop();
    m_shutdown = true;

    return true;
}

bool XZInterface::SetAppPath(const std::string &fullpath)
{
    size_t tokPos = fullpath.find_last_of('\\');

    if (tokPos == std::string::npos)
        return false;

    m_appPath = fullpath.substr(0, tokPos);
    return true;
}

void XZInterface::ShowFileList()
{
    FileListTraveling([](std::string &&str)
    {
        std::cout << stringFormat("input file: %s\n", str);
        return true;
    });
}

void XZInterface::ReadOpt()
{
    m_iniFile = std::make_unique<CIniManager>(m_appPath + '\\' + "lzcompression.txt");

    std::string isCreateDir;

    m_iniFile->GetValue("SETTING", "MakeDir", isCreateDir);
    m_isCreateDir = (isCreateDir == "true") ? true : false;
}

bool XZInterface::IsNxz()
{
    size_t iMedSep = m_currentfile.find_last_of('.');

    return m_currentfile.substr(iMedSep ? iMedSep + 1 : 0) == "nxz";
}

bool XZInterface::InnerProgress(std::string &&filename)
{
    m_currentfile = std::forward<std::string>(filename);
    std::basic_ifstream<uint8_t> file(m_currentfile, std::ios::binary);

    if (!file.is_open())
        return false;
    file >> std::noskipws;

    using UCharIteratorType = std::istream_iterator<uint8_t, uint8_t>;
    std::vector<uint8_t> srcstream = std::vector<uint8_t>(UCharIteratorType(file), UCharIteratorType());

    m_compress = std::make_unique<LzCompression>();
    m_compress->OnProgressHit().Connection(&XZInterface::NotificationProgress, this);    

    m_progPercent = 0;
    bool isNxz = IsNxz();

    if (!(isNxz ? m_compress->Decompress(srcstream) : m_compress->Compress(srcstream)))
        return false;

    return WriteOutput(!isNxz);
}

std::string XZInterface::OnlyFilename()
{
    uint32_t pos = m_currentfile.find_last_of('.');
    uint32_t sep = m_currentfile.find_last_of('\\');

    return m_currentfile.substr((sep != 0) ? sep + 1 : 0, pos - sep - 1);
}

std::string XZInterface::OnlyFileExtension()
{
    uint32_t pos = m_currentfile.find_last_of('.');

    return m_currentfile.substr(pos);
}

bool XZInterface::FileListTraveling(std::function<bool(std::string &&)> &&in)
{
    for (std::string str : m_filelist)
    {
        if (!in(std::move(str)))
            return false;
    }
    return true;
}

bool XZInterface::WriteOutput(bool compressMode)
{
    std::string filename = OnlyFilename();
    std::string rootPath = m_appPath;

    if (m_isCreateDir)
    {
        rootPath += stringFormat("\\%s", filename);
        NAMESPACE_FILESYSTEM::create_directory(rootPath);
        NAMESPACE_FILESYSTEM::copy_file(m_currentfile, stringFormat("%s\\%s%s", rootPath, filename, OnlyFileExtension()));
    }

    std::basic_ofstream<uint8_t> file(stringFormat("%s\\%s.%s", rootPath, filename, (compressMode ? "nxz" : "uxz")), std::ios::binary);

    if (!file.is_open())
        return false;

    std::vector<uint8_t> deststream;

    m_compress->GetResult(deststream);
    file.write(deststream.data(), deststream.size());

    return true;
}

void XZInterface::ShowPerformance()
{
    std::this_thread::sleep_for(std::chrono::milliseconds(100));
    int cur = 0, max = 0;

    {
        std::lock_guard<std::mutex> locker(m_lock);
        if (m_procRateList.empty())
            return;

        cur = std::get<0>(m_procRateList.back());
        max = std::get<1>(m_procRateList.back());        

        m_procRateList.clear();
    }

    int percent = (cur * 100) / max;

    if (m_progPercent != percent)
    {
        m_progPercent = percent;
        std::cout << stringFormat("progress %d of %d (%d %%)\n", cur, max, percent);
    }
    //m_onStoppedPerformance.Emit(true);
}

void XZInterface::NotificationProgress(int cur, int max)
{
    if (m_shutdown)
        return;

    {
        std::lock_guard<std::mutex> locker(m_lock);

        m_procRateList.emplace_back(cur, max);
    }
}

void XZInterface::StopPerfomance(bool stat)
{
    std::cout << stringFormat("stop performance thread- stat: %s\n", stat ? "true" : "false");
}

