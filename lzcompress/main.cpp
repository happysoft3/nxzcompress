﻿// nxzcompression.cpp : 이 파일에는 'main' 함수가 포함됩니다. 거기서 프로그램 실행이 시작되고 종료됩니다.
//

#include "runclock.h"
#include "xzinterface.h"
#include "eventWorker.h"
#include "stringhelper.h"

#include <iostream>

using namespace _StringHelper;

int main(int argc, char *argv[])
{
    if (argc < 2)
        return 0;

    EventWorker::Instance().Start();

    {
        RunClock clock;
        XZInterface xz(&argv[1], argc - 1);

        xz.SetAppPath(argv[0]);
        xz.ReadOpt();
        bool res = xz.RunInterface();

        if (!res)
            std::cout << "get an error\n";
        else
        {
            xz.ShowFileList();
            std::cout << stringFormat("success %d items\n", argc - 1);
        }
    }

    std::getchar();
    EventWorker::Instance().Stop();

    return 0;
}

//class Aclass : public CCObject
//{
//private:
//    int m_data;
//
//public:
//    Aclass(int n)
//    {
//        m_data = n;
//    }
//    DECLARE_SIGNAL(OnHit, int)
//};
//
//class Bclass : public CCObject
//{
//private:
//    int m_data;
//
//public:
//    Bclass(int n)
//    {
//        m_data = n;
//    }
//    void SlotOnHit(int n)
//    {
//        std::cout << (m_data * n);
//    }
//};
//
//int main(int argc, char **argv)
//{
//    EventWorker::Instance().Start();
//        std::unique_ptr<Bclass> bc(new Bclass(10));
//    {
//        std::unique_ptr<Aclass> ac(new Aclass(99));
//
//        ac->OnHit().Connection(&Bclass::SlotOnHit, bc.get());
//        EventWorker::Instance().AppendTask(&ac->OnHit(), 50);
//        ac.reset();
//    }
//    std::cout << "ok?!";
//    std::getchar();
//    EventWorker::Instance().Stop();
//    return 0;
//}
