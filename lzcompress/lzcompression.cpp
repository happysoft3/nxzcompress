
#include "lzcompression.h"
#include "cbitwriter.h"
#include "bitreader.h"
#include "eventWorker.h"

#include <map>
#include <set>
#include <algorithm>
#include <iterator>
#include <iostream>

static constexpr int s_lengthTable[] =
{
    1, 0x008,
    2, 0x00A,
    3, 0x00E,
    4, 0x016,
    5, 0x026,
    6, 0x046,
    7, 0x086,
    8, 0x106
};
static constexpr int s_distanceTable[] =
{
    0, 0x00,
    0, 0x01,
    1, 0x02,
    2, 0x04,
    3, 0x08,
    4, 0x10,
    5, 0x20,
    6, 0x40
};

static constexpr int s_symbIndexTable[] = {
    0x00000002, 0x00000000,
    0x00000003, 0x00000004,
    0x00000003, 0x0000000C,
    0x00000004, 0x00000014,
    0x00000004, 0x00000024,
    0x00000004, 0x00000034,
    0x00000004, 0x00000044,
    0x00000004, 0x00000054,
    0x00000004, 0x00000064,
    0x00000004, 0x00000074,
    0x00000004, 0x00000084,
    0x00000004, 0x00000094,
    0x00000004, 0x000000A4,
    0x00000005, 0x000000B4,
    0x00000005, 0x000000D4,
    0x00000005, 0x000000F4
};

LzCompression::LzCompression(int symbols, int windowsize)
	: m_symbols(symbols), m_windowsize(windowsize),
    m_rebuildcount(m_windowsize/4)
{
	m_symbolcounts.resize(m_symbols);
	m_lzposwindow = 0;
    m_pInputptr = nullptr;
}

LzCompression::LzCompression()
	: LzCompression(274, 0x10000)
{ }

LzCompression::~LzCompression()
{ }

void LzCompression::BuildInitialAlphabet()
{
	m_symbolAlphabets.resize(m_symbols);

	int wrpos = -1;

	while ((++wrpos) < 16)
		m_symbolAlphabets[wrpos] = wrpos + 0x100;
	m_symbolAlphabets[wrpos++] = 0;
	m_symbolAlphabets[wrpos++] = 0x20;
	m_symbolAlphabets[wrpos++] = 0x30;
	m_symbolAlphabets[wrpos++] = 0xff;

	std::set<int> dupchecker;

	std::copy_n(m_symbolAlphabets.begin(), wrpos, std::insert_iterator<std::set<int>>(dupchecker, dupchecker.end()));

	IndexOfSymbolsRev(m_symbolAlphabets, dupchecker);
}

void LzCompression::RebuildAlphabet()
{
	std::map<int, int> buildmap;
	int rep = m_symbols;

	while ((--rep) >= 0)
		buildmap.emplace(MakeIntWithWord(m_symbolcounts[rep], rep), rep);

	std::transform(buildmap.begin(), buildmap.end(), m_symbolAlphabets.rbegin(), [](const auto &elm) { return elm.second; });
}

void LzCompression::FrontWork(int &matchpos)
{
	int lzwindowstart = m_workpos - m_windowsize + 1;

	if (lzwindowstart < 0)
		lzwindowstart = 0;
	int lzsearchpos = m_workpos;

	while (lzwindowstart <= lzsearchpos)
	{
		int seq = *reinterpret_cast<const int *>(&m_pInputptr[m_workpos]);
		int matchlen = 0;

		while (*reinterpret_cast<const int *>(&m_pInputptr[lzsearchpos + matchlen]) == seq)
		{
			if (((lzsearchpos + matchlen) >= m_workpos) || (matchlen >= 516) || (m_workpos + matchlen + 4) >= m_srcLength)
				break;
			matchlen += 4;
			seq = *reinterpret_cast<const int *>(&m_pInputptr[m_workpos + matchlen]); // Next part of the sequence
		}

		if (matchlen > m_longestMatchLen)
		{
			m_longestMatchLen = matchlen;
            matchpos = lzsearchpos;

			if (matchlen >= 516)
				break;
		}
		lzsearchpos -= 4;
	}
}

void LzCompression::RepeatingFoundSequence()
{
	if (m_longestMatchLen <= 11)
        m_cursymb = 0x100 + (m_longestMatchLen - 4);
	else
	{
		int tabpos = -1;
		int reflen;
		while ((++tabpos) < 8)
		{
			reflen = s_lengthTable[(tabpos << 1) + 1] + (1 << s_lengthTable[tabpos << 1]) - 1;
			if (m_longestMatchLen - 4 <= reflen)
				break;
		}
        m_cursymb = 0x108 + tabpos;
	}
}

bool LzCompression::SearchForHuffmanAlphabet()
{
	int huffmanpos = 0;

	while (huffmanpos <= m_symbols)
	{
		if (m_symbolAlphabets[huffmanpos] == m_cursymb)
			break;
		++huffmanpos;
	}

	int tabpos = 0;
	int reflen, refoff;

	for (; tabpos < 16; ++tabpos)
	{
		reflen = (1 << s_symbIndexTable[tabpos << 1]) - 1;
		refoff = s_symbIndexTable[(tabpos << 1) + 1];

		if (huffmanpos >= refoff)
		{
			if (reflen >= (huffmanpos - refoff))
				break;
		}
		else
		{
			std::cout << "Error:: Unable to encode symbol: " << m_cursymb << std::endl;
			return false;
		}
	}
	m_bitwr->Write(tabpos, 4);
	m_bitwr->Write(huffmanpos - refoff, s_symbIndexTable[tabpos << 1]);
	return true;
}

void LzCompression::SymbolEqual()
{
	RebuildAlphabet();

	int rep = m_symbols;

	while (rep > 0)
		m_symbolcounts[--rep] >>= 1;

	for (int u = 0; u < 32; u += 2)
	{
		int back = s_symbIndexTable[u];

		if (u >= 2)
			back -= s_symbIndexTable[u - 2];
		while ((back--) > 0)
			m_bitwr->Write(0);
		m_bitwr->Write(1);
	}
}

void LzCompression::SymbolGreater(int wndoff)
{
	m_workpos += m_longestMatchLen;

	if (m_cursymb >= 0x108)
		m_bitwr->Write(
			m_longestMatchLen - s_lengthTable[((m_cursymb - 0x108) << 1) + 1] - 4, s_lengthTable[(m_cursymb - 0x108) << 1]);

	int tabpos = -1;
	while ((++tabpos) < 8)
	{
		int reflen = ((s_distanceTable[(tabpos << 1) + 1]) << 9) + (1 << (9 + s_distanceTable[tabpos << 1])) - 1;

		if (wndoff <= reflen)
			break;
	}

	m_bitwr->Write(tabpos, 3);
	m_bitwr->Write(wndoff - (s_distanceTable[(tabpos << 1) + 1] << 9), s_distanceTable[tabpos << 1] + 9);
}

bool LzCompression::CompressImpl(const uint8_t *inputptr, size_t length)
{
    m_pInputptr = inputptr;
    m_srcLength = length;

    m_bitwr = std::make_unique<CBitWriter<uint8_t>>(length);

    m_cursymb = 0;
    m_workpos = 0;
    m_longestMatchLen = 0;

    int cRebuildcount = m_rebuildcount;
    int longestMatchPos = 0;

    while (m_workpos < m_srcLength)
	{
		if ((cRebuildcount--) < 0)
		{
            m_cursymb = 0x110;
            cRebuildcount = m_rebuildcount;
		}
		else
		{
			m_longestMatchLen = 0;
            if (m_workpos % 8 == 0)
				FrontWork(longestMatchPos);
            if (m_longestMatchLen >= 4)
                RepeatingFoundSequence();
            else
                m_cursymb = m_pInputptr[m_workpos++];
		}
		++m_symbolcounts[m_cursymb];
		if (!SearchForHuffmanAlphabet())
			return false;
		if (m_cursymb == 0x110)
			SymbolEqual();
		else if (m_cursymb >= 0x100)
			SymbolGreater(m_workpos - longestMatchPos);

		EventWorker::Instance().AppendTask(&m_OnProgressHit, m_workpos, m_srcLength);
		//m_OnProgressHit.Emit(m_workpos, m_srcLength);
	}

	m_bitwr->EndWrite();
	return true;
}

void LzCompression::DecompGetSymbol(const std::vector<int> &symbIndexTb)
{
	int code = m_bitrd->Read(4);
	int bits = symbIndexTb[code << 1];
	int offset = symbIndexTb[(code << 1) + 1];
	int idx = m_bitrd->Read(bits) + offset;

    m_cursymb = m_symbolAlphabets[idx];
	++m_symbolcounts[m_cursymb];
}

void LzCompression::DecompSymbLess()
{
    m_outputstream[m_workpos++] = static_cast<uint8_t>(m_cursymb);
    m_lzwindow[(m_lzposwindow++) % m_windowsize] = m_cursymb;
}

void LzCompression::DecompSymbExactly(std::vector<int> &symbIndexTb)
{
	RebuildAlphabet();

	int rep = m_symbols;

	while ((rep--) > 0)
		m_symbolcounts[rep] = m_symbolcounts[rep] >> 1;

	int bits = 0;
	int length = 0;
	int offset = 0;

	int secdrep = 16;
	while ((secdrep--) > 0)
	{
		while (m_bitrd->Readbit() == 0)
			++bits;
        symbIndexTb[length++] = bits;
        symbIndexTb[length++] = offset;
		offset += (1 << bits);
	}
}

void LzCompression::DecompSymbElse()
{
	int length = 4;

	if (m_cursymb < 0x108)
		length += (m_cursymb - 0x100);
	else
	{
		int idx = m_cursymb - 0x108;

		length += ((s_lengthTable[(idx << 1) + 1]) + m_bitrd->Read(s_lengthTable[idx << 1]));
	}

	int code = m_bitrd->Read(3);
    int bits = s_distanceTable[code << 1];
    int offset = s_distanceTable[(code << 1) + 1] << 9;
	int distance = offset + m_bitrd->Read(bits + 9);
	uint32_t idx = m_lzposwindow - distance;
	int rep = -1;

	while ((++rep) < length)
	{
        m_cursymb = m_lzwindow[(idx + rep) % m_windowsize];

        m_outputstream[m_workpos++] = static_cast<uint8_t>(m_cursymb);

        m_lzwindow[(m_lzposwindow++) % m_windowsize] = m_cursymb;
	}

}

bool LzCompression::DecompressImpl(const uint8_t *inputptr, uint32_t length)
{
    if (static_cast<int>(length) < 4)
        return false;

    uint32_t destlength = *reinterpret_cast<const uint32_t *>(inputptr);

    m_pInputptr = inputptr;

    m_outputstream.resize(destlength);
    m_lzwindow.resize(m_windowsize);
    m_bitrd = std::make_unique<BitReader<uint8_t>>();

    m_bitrd->SetBuffer(inputptr + 4);

    std::vector<int> symbIndexTb(sizeof(s_symbIndexTable) / sizeof(s_symbIndexTable[0]));

    std::copy_n(s_symbIndexTable, symbIndexTb.size(), symbIndexTb.begin());

	while (m_workpos < static_cast<int>(destlength))
	{
		DecompGetSymbol(symbIndexTb);
		if (m_cursymb < 0x100)
			DecompSymbLess();
		else if (m_cursymb == 0x110)
			DecompSymbExactly(symbIndexTb);
		else if (m_cursymb >= 0x111)
			return false;
		else
			DecompSymbElse();

		EventWorker::Instance().AppendTask(&m_OnProgressHit, m_workpos, destlength);
	}
	return true;
}

void LzCompression::completedCompress()
{
    m_bitwr->GetResult(m_outputstream);
}